<?php
// Require PWF Information Class
// Development Variable
define("DEVELOPMENT", true);
// Project State
define("PROJECT_MODE", 2);
// Verbose Level Variable
/**
 * 0 - Normal
 * 1 - Normal (in PWF meaning you can add your own in views or anything else)
 * 2 - Verbose
 * 3 - More Verbose
 * 255 - Verbose Debug Mode
 */
define("VERBOSE", 3);
/*************** Database ***************/
if(DEVELOPMENT) # Test & Dev
{
    // Database HostName
    define('DB_HOST', 'localhost');
    // Database User
    define('DB_USER', '');
    // Database Password
    define('DB_PASS', '');
    // Database Name
    define('DB_NAME', '');
}
else
{
    // Database HostName
    define('DB_HOST', 'localhost');
    // Database Name
    define('DB_NAME', '');
    // Database User
    define('DB_USER', '');
    // Database Password
    define('DB_PASS', '');
}
/*************** PATH ***************/
// Application Root
define("APPROOT", dirname(dirname(__FILE__)));
if(DEVELOPMENT) # Test & Dev
{
    // URL Root
    define("URLROOT", '');
}
else
{
    // URL Root
    define("URLROOT", '');
}
/************** Project Information *************/
// Project Version
define("PROJECT_VERSION", "0.0dev0");
define("PROJECT_FULL_VERSION", "0.0dev0+20181108222742");