<?php
/**
 * Pages Controller
 * This controller provides routes for main origin.
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * Pages Controller
 * This controller provides routes for main origin.
 */
class Pages extends APIController
{
    /**
     * Pages constructor.
     */
    public function __construct()
    {
    }

    /**
     * Main Route
     * /
     * This route provides you Main Page of whole application.
     */
    public function index()
    {
        $data = [];
        $this->respondJSON($data);
    }
}