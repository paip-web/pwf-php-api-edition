<?php
/**
 * Test Model Class
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * Test Model Class
 */
class TestModel
{
    /**
     * TestModel Constructor
     */
    public function __construct()
    {
    }
    const filename = APPROOT.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."data".DIRECTORY_SEPARATOR."tests.json";
    public function all()
    {
        return json_decode(file_get_contents(self::filename));
    }
    public function add($fndata)
    {
        $data = $this->all();
        $data[] = $fndata;
        $this->save($data);
        return $this->all();
    }
    public function save($data)
    {
        return file_put_contents(self::filename, json_encode($data));
    }
}