<?php
/**
 * Bootstraping Script
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */
// Load Config
require_once "config/config.php";

// Autoload Core Libraries
spl_autoload_register(function($className){
    require_once "libraries/".$className.".php";
});